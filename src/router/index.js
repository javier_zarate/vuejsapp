import Vue from 'vue'
import Router from 'vue-router'
import AppLayoutAdmin from '../components/admin/AppLayout'
import AppLayoutOperator from '../components/operator/AppLayout'
import AuthService from "./../msal_auth/authad"
import store from "./../store/index"

Vue.use(Router)
const EmptyParentComponent = {
  template: '<router-view></router-view>',
}
const demoRoutes = []
var null_value = null;
const router = new Router({
  mode: process.env.VUE_APP_ROUTER_MODE_HISTORY === 'true',
  routes: [
    ...demoRoutes,
    {
      path: '/',
      redirect: { name: 'auth' },
    },
    {
      path: '*',
      name: 'page_error',
      meta: {
        notfound: true
      }
    },
    {
      path: '/auth',
      name: 'auth',
      component: () => import('../components/auth/AuthLayout.vue'),
      meta: {
        forgetlogin: true
      }
    },
    {
      name: 'Admin',
      path: '/admin',
      component: AppLayoutAdmin,
      redirect: { name: 'admin_dashboard'},
      children: [
        {
          name: 'admin_dashboard',
          path: '/admin/dashboard',
          component: () => import('../components/dashboard/Dashboard.vue'),
          default: true,
          meta: {
            beforeAdminDashboard: true
          }
        },
        {
          name: 'admin_parameters',
          path: '/admin/parameters',
          component: EmptyParentComponent,
          redirect: { name: 'admin_parameter_coin'},        
          children: [
            {
              name: 'admin_parameter_coin',
              path: '/admin/parameters/coin',
              component: () => import('../components/parameters/editparameters/Coin.vue'),
              meta: {
                beforeAdminParameterCoin: true
              }
            },
            {
              name: 'admin_parameter_bank',
              path: '/admin/parameters/bank',
              component: () => import('../components/parameters/editparameters/Bank.vue'),
              meta: {
                beforeAdminParameterBank: true
              }
            },
            {
              name: 'admin_parameter_channel',
              path: '/admin/parameters/channel',
              component: () => import('../components/parameters/editparameters/Channel.vue'),
              meta: {
                beforeAdminParameterChannel: true
              }
            },
          ],
          meta: {
            beforeAdminParameters: true
          }
        },
        {
          name: 'admin_tables',
          path: '/admin/tables',
          component: EmptyParentComponent,
          redirect: { name: 'admin_DailyData'},          
          children: [
            {
              name: 'admin_HistoricData',
              path: '/admin/tables/historicdata',
              component: () => import('../components/historic_data/HistoricData.vue'),
              meta: {
                beforeAdminHistoricData: true
              }
            },
            {
              name: 'admin_DailyData',
              path: '/admin/tables/dailydata',
              component: () => import('../components/daily_data/DailyData.vue'),
              meta: {
                beforeAdminDailyData: true
              }
            },
          ],
          meta: {
            beforeAdminTable: true
          }
        },
      ],
      meta: {
        beforeAdmin: true
      }
    },
    {
      name: 'Operator',
      path: '/operator',
      component: AppLayoutOperator,
      redirect: {name: 'operator_dashboard'},      
      children: [
        {
          name: 'operator_dashboard',
          path: '/operator/dashboard',
          component: () => import('../components/dashboard/Dashboard.vue'),
          default: true,
          meta: {
            beforeOperatorDashboard: true
          }
        },
        {
          name: 'operator_tables',
          path: '/operator/tables',
          component: EmptyParentComponent,
          children: [
            {
              name: 'operator_HistoricData',
              path: '/operator/tables/historicdata',
              component: () => import('../components/historic_data/HistoricData.vue'),
              meta: {
                beforeOperatorHistoricData: true
              }
            },
            {
              name: 'operator_DailyData',
              path: '/operator/tables/dailydata',
              component: () => import('../components/daily_data/DailyData.vue'),
              meta: {
                beforeOperatorDailyData: true
              }
            },
          ],
          meta: {
            beforeOperatorTable: true
          }
        },
      ],
      meta: {
        beforeOperator: true
      }
    },
    {
      path: '/logout',
      name: 'logout' ,
      meta: {
        logout: true
      }
    }
  ]
})

router.beforeEach((to, from, next) =>{
  var stored_user = new AuthService();
  var user_saved = stored_user.getlogged();
  var read_role = store.state.role_stored;
  console.log(user_saved);
  console.log(read_role);


  //verificar roles en cada cambio de ruta
  /*if(user_saved){
    console.log(user_saved.displayableId);
  }
  console.log(read_role);
  */  
  if(to.matched.some(record =>record.meta.beforeAdmin) || to.matched.some(record =>record.meta.beforeAdminDashboard) || to.matched.some(record =>record.meta.beforeAdminParameters) || to.matched.some(record =>record.meta.beforeAdminParameterCoin) || to.matched.some(record =>record.meta.beforeAdminParameterBank) || to.matched.some(record =>record.meta.beforeAdminParameterChannel) || to.matched.some(record =>record.meta.beforeAdminTable) || to.matched.some(record =>record.meta.beforeAdminDailyData) || to.matched.some(record =>record.meta.beforeAdminHistoricData)){
    if(!user_saved){
      console.log('no hay usuario admin!');
      next({
        name: 'auth'
      })
    }
    else {  
      if(read_role=="admin"){
        next();
      }
      else if(read_role=="operator"){
        console.log('acceso restringido por rol');
        next({
          name: 'auth'
        })
      }
      else{
        console.log('usuario sin rol');
        next({
          name: 'auth'
        })
      }
     next();
    }
  }
  else if(to.matched.some(record =>record.meta.beforeOperator)||to.matched.some(record =>record.meta.beforeOperatorDashboard)||to.matched.some(record =>record.meta.beforeOperatorTable)||to.matched.some(record =>record.meta.beforeOperatorHistoricData)||to.matched.some(record =>record.meta.beforeOperatorDailyData)){
    if(!user_saved){
      console.log('no hay usuario operador!');
      next({
        name: 'auth'
      })
    }
    else {
      if(read_role=="operator"){
        next();
      }
      else if(read_role=="admin"){
        console.log('acceso restringido por rol');
        next({
          name: 'auth'
        })
      }
      else{
        console.log('usuario sin rol');
        next({
          name: 'auth'
        })
      }
      next();
    }
  }

  else if(to.matched.some(record =>record.meta.forgetlogin)){
    if(user_saved){
      if(read_role=="admin"){
        next({
          name: 'admin_dashboard'
        })
      }
      else if(read_role=="operator"){
        next({
          name: 'operator_dashboard'
        })
      }
      else{
        next();
      }
    }
    else{
      next();
    }
  }
  else if(to.matched.some(record =>record.meta.logout)){
    if(user_saved){
      store.commit('save_user',null_value);
      store.commit('save_role',null_value);
      stored_user.logout();
    }
    else{
      next({
        name:'auth'
      });
    }
  }
  else if(to.matched.some(record =>record.meta.notfound)){
    next({
      name: 'auth'
    });
  }
  else{
    next();
  }
})

export default router
