import Vue from "vue";
import Vuex from "vuex";
import VuexI18n from "vuex-i18n";
import app from "./modules/app";
import * as getters from "./getters";
//vue-persist
//import CreatePersistedState from 'vuex-persist'
//vue-persistedstate
import CreatePersistedState from "vuex-persistedstate";
import SecureLS from "secure-ls";

const ls = new SecureLS({ isCompression: false });
//ls.removeAll();
Vue.use(Vuex);

//vue-persist
/*const vuexLocal = new CreatePersistedState({
  storage: window.localStorage,
});
*/
const store = new Vuex.Store({
  strict: true, // process.env.NODE_ENV !== 'production',
  getters,
  modules: {
    app
  },
  state: {
    user_stored: null,
    role_stored: null,
    name_stored: null,
    bank_stored: [
      /*
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      {code_bank : null, name_bank : null, entidad_bank : null, state_bank : null},
      */
      {
        code_bank: null,
        name_bank: null,
        entidad_bank: null,
        state_bank: null
      }
    ],
    typetransfer_stored: [
      {
        code_typetransfer: "320",
        name_typetransfer: "Abono Cta",
        state_typetransfer: "ACTIVO"
      },
      {
        code_typetransfer: "325",
        name_typetransfer: "Pago Tarj. Cred",
        state_typetransfer: "ACTIVO"
      }
    ],
    process_stored: [
      { code_process: "800000", name_process: "Consulta" },
      { code_process: "490000", name_process: "Transferencia" }
    ],
    responsecodelong_stored: [
      { code_responsecodelong: "0101", name_responsecodelong: "0101" },
      { code_responsecodelong: "0137", name_responsecodelong: "0137" },
      { code_responsecodelong: "9915", name_responsecodelong: "9915" },
      { code_responsecodelong: "9916", name_responsecodelong: "9916" }
    ],
    channel_stored: [
      /*
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      {code_channel : null, name_channel : null, entidad_channel : null,state_channel : null},
      */
      {
        code_channel: null,
        name_channel: null,
        entidad_channel: null,
        state_channel: null
      }
    ],
    coin_stored: [
      //{code_coin : null, name_coin : null, symbol_coin : null,state_coin : null},
      { code_coin: null, name_coin: null, symbol_coin: null, state_coin: null }
    ],
    originresponsecode_stored: [
      { code_originresponsecode: "001", name_originresponsecode: "001" },
      { code_originresponsecode: "002", name_originresponsecode: "002" },
      { code_originresponsecode: "100", name_originresponsecode: "100" }
    ]
  },
  getters: {
    get_banks_stored(state) {
      return state.bank_stored;
    },
    get_typetransfer_stored(state) {
      return state.typetransfer_stored;
    },
    get_process_stored(state) {
      return state.process_stored;
    },
    get_responsecodelong_stored(state) {
      return state.responsecodelong_stored;
    },
    get_channels_stored(state) {
      return state.channel_stored;
    },
    get_coins_stored(state) {
      return state.coin_stored;
    },
    get_originresponsecode_stored(state) {
      return state.originresponsecode_stored;
    },
    get_role_stored(state) {
      return state.role_stored;
    },
    get_user_stored(state) {
      return state.user_stored;
    },
    get_name_stored(state) {
      return state.name_stored;
    }
  },
  mutations: {
    save_user(state, user_logged) {
      state.user_stored = user_logged;
    },
    save_role(state, role_asigned) {
      state.role_stored = role_asigned;
    },
    save_coin(state, coin_data) {
      //after read json data
      state.coin_stored = coin_data;
    },
    save_bank(state, bank_data) {
      //after read json data
      state.bank_stored = bank_data;
    },
    save_channel(state, channel_data) {
      //after read json data
      state.channel_stored = channel_data;
    },
    add_single_channel(state, single_channel) {
      state.channel_stored.push({
        code_channel: single_channel[0],
        name_channel: single_channel[1],
        entidad_channel: single_channel[2],
        state_channel: single_channel[3]
      });
    },
    save_name(state, data) {
      state.name_stored = data;
    }
  },
  plugins: [
    CreatePersistedState({
      storage: {
        getItem: key => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: key => ls.remove(key)
      }
    })
    //vuex-persist
    //vuexLocal.plugin
  ]
});

Vue.use(VuexI18n.plugin, store);

export default store;
