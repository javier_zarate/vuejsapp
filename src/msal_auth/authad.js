import router from "./../router/index"
import * as Msal from 'msal';
var user = null;
export default class AuthService {
  //default constructor
  constructor(){
    var redirectURI= "https://wapceu2ttibd00.azurewebsites.net/"
    this.applicationConfig= {
      clientID: "c6ffec42-1195-469f-9c2e-1f490b132bf9",
      graphScopes: ["user.read"]
    };
    this.app = new Msal.UserAgentApplication(
      this.applicationConfig.clientID,
      '',
      () => {
        // here the callback for login redirect
      },
      {
      redirectURI
      }
    );
  }
  login() {
    return this.app.loginPopup(this.applicationConfig.graphScopes)
      .then(
      idToken => {
        user = this.app.getUser();
        if (user) {
          return user;
        } 
        else {
          return null;
        }
      },
      () => {
        return null;
      }
    )
    .catch(error =>{
      console.log(error);
    });
  };
  getlogged() {
    const userlogged = this.app.getUser();
    if (userlogged) {
      return userlogged;
    } 
    else {
      return null;
    }
  };
  logout(){
    this.app.logout();
  };
  getToken() {
    return this.app.acquireTokenSilent(this.applicationConfig.graphScopes)
    .then(
      accessToken => {
        return accessToken;
      },
      err => {
        console.log(err);
      }
    )
    .catch(
      error =>{
        console.log(error)
      }
    );
  };
}  





