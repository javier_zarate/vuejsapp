export const navigationRoutes = {
  root: {
    name: '/',
    displayName: 'navigationRoutes.home',
  },
  routes: [
    {
      name: 'admin_dashboard',
      displayName: 'menu.dashboard',
      meta: {
        iconClass: 'vuestic-iconset vuestic-iconset-dashboard',
      },
    },
    {
      name: 'admin_parameters',
      displayName: 'Parámetros',
      meta: {
        iconClass: 'vuestic-iconset vuestic-iconset-forms',
      },
      disabled: true,
      children: [
        {
          name: 'admin_parameter_coin',
          displayName: 'Parám. Moneda',
        },
        {
          name: 'admin_parameter_bank',
          displayName: 'Parám. Banco',
        },
        {
          name: 'admin_parameter_channel',
          displayName: 'Parám. Canal',
        }
      ],
    },
    {
      name: 'admin_tables',
      displayName: 'Tablas',
      meta: {
        iconClass: 'vuestic-iconset vuestic-iconset-tables',
      },
      children: [
        {
          name: 'admin_DailyData',
          displayName: 'Data Diaria',
        },
        {
          name: 'admin_HistoricData',
          displayName: 'Data Histórica',
        },
      ],
    },
    {
      name: 'logout',
      displayName: 'Cerrar Sesión',
      meta: {
        iconClass: 'vuestic-iconset vuestic-iconset-ui-elements',
      },
    },
  ],
}
