export const navigationRoutes = {
  root: {
    name: '/',
    displayName: 'navigationRoutes.home',
  },
  routes: [
    {
      name: 'operator_dashboard',
      displayName: 'menu.dashboard',
      meta: {
        iconClass: 'vuestic-iconset vuestic-iconset-dashboard',
      },
    },
    {
      name: 'operator_tables',
      displayName: 'Tablas',
      meta: {
        iconClass: 'vuestic-iconset vuestic-iconset-tables',
      },
      children: [
        {
          name: 'operator_DailyData',
          displayName: 'Data Diaria',
        },
        {
          name: 'operator_HistoricData',
          displayName: 'Data Histórica',
        },
      ],
    },
    {
      name: 'logout',
      displayName: 'Cerrar Sesión',
      meta: {
        iconClass: 'vuestic-iconset vuestic-iconset-ui-elements',
      },
    },
  ],
}
